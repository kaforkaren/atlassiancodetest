package com.altassian.hipchatconvert;

import com.altassian.hipchatconvert.Fragment.MainFragment;
import com.altassian.hipchatconvert.Util.ConvertFormatType;
import com.altassian.hipchatconvert.Util.ConvertJSONStringUtil;

import org.junit.Test;

import java.util.List;


import static org.junit.Assert.*;

public class MessageConvertUnitTest {

    /* Test Mention */
    @Test
    public void testMentionsReturnNonNull() throws Exception {
        List<String> mentions = ConvertJSONStringUtil.getContent("test", ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertNotNull(mentions);
        assertTrue(mentions.isEmpty());
    }

    @Test
    public void testMentionsNotAt() throws Exception {
        List<String> mentions = ConvertJSONStringUtil.getContent("test tree", ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertNotNull(mentions);
        assertTrue(mentions.isEmpty());
    }

    @Test
    public void testMentionsNormal1() throws Exception {
        final String mentionName = "chris";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("@%s How are you", mentionName),
                ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 1);
        assertTrue(mentions.get(0).equals(mentionName));
    }

    @Test
    public void testMentionNormal2() throws Exception {
        final String mentionName = "peter";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("Hello, @%s How are you",
                mentionName), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 1);
        assertTrue(mentions.get(0).equals(mentionName));
    }

    @Test
    public void testMentionNormal3() throws Exception {
        final String mentionName = "James";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("How are you @%s",
                mentionName), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 1);
        assertTrue(mentions.get(0).equals(mentionName));
    }

    @Test
    public void testMentionNonSpace1() throws Exception {
        final String mentionName = "James";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("This is no space at the end@%s",
                mentionName), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 1);
        assertTrue(mentions.get(0).equals(mentionName));
    }

    @Test
    public void testMentionEndWithNonWord1() throws Exception {
        final String mentionName = "James";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("This is no space at the beginning@%s@",
                mentionName), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 1);
        assertTrue(mentions.get(0).equals(mentionName));
    }

    @Test
    public void testMentionEndWithNonWord2() throws Exception {
        final String mentionName = "James";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("This is no space @%s!at the beginning",
                mentionName), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 1);
        assertTrue(mentions.get(0).equals(mentionName));
    }

    @Test
    public void testMentionMultiNameWithSpace() throws Exception {
        final String mentionName1 = "william";
        final String mentionName2 = "Philip";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format(" @%s @%s at the beginning",
                mentionName1, mentionName2), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 2);
        assertTrue(mentions.get(0).equals(mentionName1));
        assertTrue(mentions.get(1).equals(mentionName2));
    }

    @Test
    public void testMentionMultiNameWithoutSpace() throws Exception {
        final String mentionName1 = "william";
        final String mentionName2 = "Philip";

        List<String> mentions = ConvertJSONStringUtil.getContent(String.format("@%s@%s at the beginning",
                mentionName1, mentionName2), ConvertFormatType.MENTION_CONVERT_FORMAT);
        assertTrue(mentions.size() == 2);
        assertTrue(mentions.get(0).equals(mentionName1));
        assertTrue(mentions.get(1).equals(mentionName2));
    }

    /* Test Emoticons */
    @Test
    public void testEmoticonsReturnNonNull() throws Exception {
        List<String> emoticons = ConvertJSONStringUtil.getContent("test", ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertNotNull(emoticons);
        assertTrue(emoticons.isEmpty());
    }

    @Test
    public void testEmoticonsNoIcons() throws Exception {
        List<String> mentions = ConvertJSONStringUtil.getContent("test tree",
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertNotNull(mentions);
        assertTrue(mentions.isEmpty());
    }

    @Test
    public void testEmoticonsNormal1() throws Exception {
        final String emoticon = "gangnamstyle";

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Hello, (%s)", emoticon),
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 1);
        assertTrue(emoticons.get(0).equals(emoticon));
    }

    @Test
    public void testEmoticonsNormal2() throws Exception {
        final String emoticon = "gangnamstyle";

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("(%s). rere", emoticon),
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 1);
        assertTrue(emoticons.get(0).equals(emoticon));
    }

    @Test
    public void testEmoticonsWithNonAlphanumeric1() throws  Exception {
        final String emoticon = "er2e";

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Hello, (%s)", emoticon),
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() ==0);
    }

    @Test
    public void testEmoticonsWithNonAlphanumeric2() throws Exception {
        final String emoticon = "er%e";

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Hello, (%s)", emoticon),
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 0);
    }

    @Test
    public void testEmoticonsBracket1() throws Exception {
        final String emoticon = "ftfy";
        final String emoticonTestString = String.format("(testing(%s)", emoticon);

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Hello, (%s)",
                emoticonTestString), ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 1);
        assertTrue(emoticons.get(0).equals(emoticon));
    }

    @Test
    public void testEmoticonsBracket2() throws Exception {
        final String emoticon = "gates";
        final String emoticonTestString = String.format("((testing(%s)(", emoticon);

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Hello, (%s)",
                emoticonTestString), ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 1);
        assertTrue(emoticons.get(0).equals(emoticon));
    }

    @Test
    public void testEmoticonsBracket3() throws Exception {
        final String emoticon = "gates";
        final String emoticonTestString = String.format("((testing(%s()(", emoticon);

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Hello, (%s)", emoticonTestString),
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 0);
    }

    @Test
    public void testEmoticonsEmpty() throws Exception {
        List<String> emoticons = ConvertJSONStringUtil.getContent("Hello, ()",
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 0);
    }

    @Test
    public void testEmoticonsMulti1() throws Exception {
        final String emoticon1 = "fireworks";
        final String emoticon2 = "feelsgoodman";

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("(%s) (%s) Test",
                emoticon1, emoticon2), ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 2);
        assertTrue(emoticons.get(0).equals(emoticon1));
        assertTrue(emoticons.get(1).equals(emoticon2));
    }

    @Test
    public void testEmoticonsMulti2() throws Exception {
        final String emoticon1 = "jackie";
        final String emoticon2 = "greenbeer";

        List<String> emoticons = ConvertJSONStringUtil.getContent(String.format("Without space(%s)(%s) ",
                emoticon1, emoticon2), ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 2);
        assertTrue(emoticons.get(0).equals(emoticon1));
        assertTrue(emoticons.get(1).equals(emoticon2));
    }

    @Test
    public void testEmoticonsExceedLength() throws Exception {
        List<String> emoticons = ConvertJSONStringUtil.getContent("Hello, (androidtestingemoticon)",
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        assertTrue(emoticons.size() == 0);
    }

    /* Test Link */
    @Test
    public void testLinksReturnNonNull() throws Exception {
        List<String> links = ConvertJSONStringUtil.getContent("test", ConvertFormatType.LINK_CONVERT_FORMAT);
        assertNotNull(links);
        assertTrue(links.isEmpty());
    }

    @Test
    public void testLinksNormal1() throws Exception {
        String url = "http://www.smh.com";
        List<String> links = ConvertJSONStringUtil.getContent(url, ConvertFormatType.LINK_CONVERT_FORMAT);
        assertTrue(links.size() == 1);
        assertTrue(links.get(0).equals(url));
    }

    @Test
    public void testLinksNormal2() throws Exception {
        String url = "http://www.smh.com";
        List<String> links = ConvertJSONStringUtil.getContent(String.format(" %s", url),
                ConvertFormatType.LINK_CONVERT_FORMAT);
        assertTrue(links.size() == 1);
        assertTrue(links.get(0).equals(url));
    }

    @Test
    public void testLinksNormal3() throws Exception {
        String url = "http://www.smh.com";
        List<String> links = ConvertJSONStringUtil.getContent(String.format("here is the link: %s aasas", url),
                ConvertFormatType.LINK_CONVERT_FORMAT);
        assertTrue(links.size() == 1);
        assertTrue(links.get(0).equals(url));
    }

    @Test
    public void testLinksNormal4() throws Exception {
        String url = "www.smh.com";
        List<String> links = ConvertJSONStringUtil.getContent(String.format("here is the link: %s aasas", url),
                ConvertFormatType.LINK_CONVERT_FORMAT);
        assertTrue(links.size() == 1);
        assertTrue(links.get(0).equals(url));
    }

    @Test
    public void testMultiUrls() throws Exception {
        String url1 = "www.smh.com";
        String url2 = "http://www.yahoo.com";
        List<String> links = ConvertJSONStringUtil.getContent(String.format("here is the link: %s %s aasas", url1, url2),
                ConvertFormatType.LINK_CONVERT_FORMAT);
        assertTrue(links.size() == 2);
        assertTrue(links.get(0).equals(url1));
        assertTrue(links.get(1).equals(url2));
    }

    @Test
    public void testHtmlTitleNormal1() throws Exception {
        String title = "Google";
        List<String> titles = ConvertJSONStringUtil.getContent(String.format("<title>%s</title>", title),
                ConvertFormatType.HTML_TAG_TITLE_FORMAT);
        assertTrue(titles.size() == 1);
        assertTrue(titles.get(0).equals(title));
    }

    @Test
    public void testHtmlTitleNormal2() throws Exception {
        String title = "Google+ title";
        List<String> titles = ConvertJSONStringUtil.getContent(String.format("<title>%s</title>", title),
                ConvertFormatType.HTML_TAG_TITLE_FORMAT);
        assertTrue(titles.size() == 1);
        assertTrue(titles.get(0).equals(title));
    }

    @Test
    public void testHtmlTitleNormal3() throws Exception {
        String title = "Google+ title";
        List<String> titles = ConvertJSONStringUtil.getContent(String.format("<title>%s</title>", title),
                ConvertFormatType.HTML_TAG_TITLE_FORMAT);
        assertTrue(titles.size() == 1);
        assertTrue(titles.get(0).equals(title));
    }


}
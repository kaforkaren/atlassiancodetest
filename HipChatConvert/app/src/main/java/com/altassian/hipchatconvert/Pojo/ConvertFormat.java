package com.altassian.hipchatconvert.Pojo;

import lombok.Getter;

/**
 * Model object used for storing convert information
 */
public class ConvertFormat {

    @Getter String regExp;
    @Getter int startPosition;
    @Getter int numberCharsToEnd;

    public ConvertFormat(String regExp, int startPosition, int numberCharsToEnd) {
        this.regExp = regExp;
        this.startPosition = startPosition;
        this.numberCharsToEnd = numberCharsToEnd;
    }



}

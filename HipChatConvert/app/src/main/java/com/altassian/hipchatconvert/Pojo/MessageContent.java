package com.altassian.hipchatconvert.Pojo;

import java.util.List;

/**
 * Model object used for message after conversion
 */
public class MessageContent {

    List<String> mentions;
    List<String> emoticons;
    List<Link> links;

    public MessageContent(List<String> mentions, List<String> emoticons, List<Link> links) {
        this.mentions = mentions;
        this.emoticons = emoticons;
        this.links = links;
    }
}

package com.altassian.hipchatconvert.Network;

import android.util.Pair;

import com.altassian.hipchatconvert.Util.Constants;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;
import rx.functions.Func1;

/**
 * Network Service for retrieving network
 */
public class ClientService {

    private IClientService clientService;

    private static ClientService client;

    public ClientService() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://www.smh.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        clientService = retrofit.create(IClientService.class);
    }

    public static ClientService getInstance() {
        if (client == null) {
            client = new ClientService();
        }

        return client;
    }

    private interface IClientService {
        @GET
        Observable<retrofit2.Response<ResponseBody>> fetchUrl(@Url String url);
    }

    /**
     * Method used to fetch url response
     * @param url url string
     * @return Observable contains url and response
     */
    public Observable<Pair<String, retrofit2.Response<ResponseBody>>> fetchUrl(String url) {
        if (!url.contains(Constants.URL_PROTOCOL)) {
            url = Constants.URL_PROTOCOL + "://" + url;
        }

        final String finalUrl = url;
        return clientService.fetchUrl(url).flatMap(new Func1<retrofit2.Response<ResponseBody>,
                Observable<Pair<String, retrofit2.Response<ResponseBody>>>>() {
            @Override
            public Observable<Pair<String, retrofit2.Response<ResponseBody>>> call(
                    retrofit2.Response<ResponseBody> responseResponse) {
                return Observable.just(new Pair<>(finalUrl, responseResponse));
            }
        });
    }
}

package com.altassian.hipchatconvert.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.altassian.hipchatconvert.R;
import com.altassian.hipchatconvert.ViewModel.MainViewModel;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Custom input view
 */
public class InputView extends LinearLayout {

    MainViewModel viewModel;

    @Bind(R.id.et_input) EditText inputText;
    @Bind(R.id.bt_run) Button runButton;

    public InputView(Context context) {
        super(context);
        init(context);
    }

    public InputView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_input, this);
        ButterKnife.bind(this);
    }

    /**
     * Method uses to setup view model
     * @param viewModel Contains view model data
     */
    public void setupView(MainViewModel viewModel) {
        this.viewModel = viewModel;
    }

    /**
     * Method uses to setup OnClickListener for run button
     * @param runClickListener OnClickListener for run button
     */
    public void setupListener( OnClickListener runClickListener) {
        runButton.setOnClickListener(runClickListener);

        RxTextView.afterTextChangeEvents(inputText).map(new Func1<TextViewAfterTextChangeEvent, Boolean>() {
            @Override
            public Boolean call(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                String inputString = textViewAfterTextChangeEvent.editable().toString();
                viewModel.setInputMessage(inputString);
                return Boolean.valueOf(inputString != null && !inputString.isEmpty());
            }
        }).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean b) {
                runButton.setEnabled(b);
            }
        });
    }

    /**
     * Method used to clear text
     */
    public void clearTextView() {
        inputText.setText("");
    }

}

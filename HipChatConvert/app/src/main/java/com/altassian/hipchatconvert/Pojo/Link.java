package com.altassian.hipchatconvert.Pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * Model object used for link
 */
public class Link {
    @Setter @Getter String url;
    @Setter @Getter String title;

    public Link(String url, String title) {
        this.url = url;
        this.title = title;
    }
}

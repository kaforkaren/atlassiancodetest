package com.altassian.hipchatconvert.Util;

/**
 * Constants class contains all constants used in the app
 */
public class Constants {
    private static final String MENTION_AT = "@";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";
    private static final String HTML_TITLE = "title";
    public static final String URL_PROTOCOL = "http";

    public static final String URL_REG =
            String.format("[\\s]?(%s?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w\\.-]*)*\\/?",
            Constants.URL_PROTOCOL);
    public static final String EMOTICONS_REG = String.format("\\%s[a-z]{1,15}\\%s",
            Constants.OPEN_BRACKET, Constants.CLOSE_BRACKET);
    public static final String MENTION_REG = String.format("%s[\\w]+", Constants.MENTION_AT);

    public static final String HTML_TITLE_REG = String.format("<%s>[\\w\\W]+</%s>", Constants.HTML_TITLE, Constants.HTML_TITLE);

}

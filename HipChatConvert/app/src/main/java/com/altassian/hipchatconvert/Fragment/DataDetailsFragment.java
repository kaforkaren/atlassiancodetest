package com.altassian.hipchatconvert.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altassian.hipchatconvert.Activity.DataDetailsActivity;
import com.altassian.hipchatconvert.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Fragment for display message / JSON string in full
 *
 */
public class DataDetailsFragment extends Fragment {

    @Bind(R.id.tv_display_input_title) TextView displayInput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_data_details, container, false);
        ButterKnife.bind(this, view);

        String data = getActivity().getIntent().getStringExtra(DataDetailsActivity.DATA_DISPLAY_KEY);
        if (data != null) {
            displayData(data);
        }

        return view;
    }

    /**
     * Methods used to display message / JSON string in full
     * @param data
     */
    public void displayData(String data) {
        displayInput.setText(data);
    }


}

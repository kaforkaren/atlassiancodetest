package com.altassian.hipchatconvert.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.altassian.hipchatconvert.Activity.DataDetailsActivity;
import com.altassian.hipchatconvert.Pojo.Link;
import com.altassian.hipchatconvert.Pojo.MessageContent;
import com.altassian.hipchatconvert.R;
import com.altassian.hipchatconvert.Util.ConvertFormatType;
import com.altassian.hipchatconvert.Util.ConvertJSONStringUtil;
import com.altassian.hipchatconvert.View.InputView;
import com.altassian.hipchatconvert.ViewModel.MainViewModel;
import com.google.gson.Gson;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Fragment for user input chat message
 */
public class MainFragment extends Fragment {

    private MainViewModel viewModel = new MainViewModel();

    @Bind(R.id.sv_container) ScrollView container;
    @Bind(R.id.tv_display_json_result) TextView jsonResultTextView;
    @Bind(R.id.tv_display_message) TextView messageTextView;
    @Bind(R.id.ll_input_view) InputView inputView;
    @Bind(R.id.pb_loading_spinner) ProgressBar progressBar;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        // setup buttons listeners
        setupView();

        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (inflater != null) {
            inflater.inflate(R.menu.menu_main, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_clear) {
            clearDisplay();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method used to setup view, it includes
     * - run button onclicklistener
     * - 2 display chat messages onclicklistener
     *
     */
    private void setupView() {
        inputView.setupView(viewModel);

        // run button
        View.OnClickListener runListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewModel.getInputMessage() != null &&
                        !viewModel.getInputMessage().isEmpty()) {
                    convertToMessageContent(viewModel.getInputMessage());
                }
            }
        };

        inputView.setupListener(runListener);

        jsonResultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = DataDetailsActivity.buildIntent(getContext(), jsonResultTextView.getText().toString());
                startActivity(intent);
            }
        });

        RxTextView.textChangeEvents(jsonResultTextView).subscribe(new Action1<TextViewTextChangeEvent>() {
            @Override
            public void call(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (textViewTextChangeEvent.text().length() > 0) {
                    jsonResultTextView.setEnabled(true);
                } else {
                    jsonResultTextView.setEnabled(false);
                }
            }
        });

        messageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = DataDetailsActivity.buildIntent(getContext(), messageTextView.getText().toString());
                startActivity(intent);
            }
        });

        RxTextView.textChangeEvents(messageTextView).subscribe(new Action1<TextViewTextChangeEvent>() {
            @Override
            public void call(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (textViewTextChangeEvent.text().length() > 0) {
                    messageTextView.setEnabled(true);
                } else {
                    messageTextView.setEnabled(false);
                }
            }
        });
    }

    /**
     * Method used to clear the 2 display text view
     */
    public void clearDisplay() {
        jsonResultTextView.setText("");
        messageTextView.setText("");
        inputView.clearTextView();
    }

    /**
     * Method used to update UI after user input chat message
     */
    private void updateUI() {
        jsonResultTextView.setText(viewModel.getDisplayJSON());
        messageTextView.setText(viewModel.getDisplayMessage());
    }

    /**
     * Method used to convert chat message to JSON
     * @param message chat message
     */
    public void convertToMessageContent(String message) {
        updateProgressBar(true);

        final List<String> mentions = ConvertJSONStringUtil.getContent(message,
                ConvertFormatType.MENTION_CONVERT_FORMAT);
        final List<String> emoticons = ConvertJSONStringUtil.getContent(message,
                ConvertFormatType.EMOTION_CONVERT_FORMAT);
        final List<String> linksInString = ConvertJSONStringUtil.getContent(message,
                ConvertFormatType.LINK_CONVERT_FORMAT);

        final List<Link> links = new ArrayList<>();
        if (linksInString != null && !linksInString.isEmpty()) {
            ConvertJSONStringUtil.getUrlTitle(linksInString, new Action1<Pair<String, Response<ResponseBody>>>() {
                @Override
                public void call(Pair<String, retrofit2.Response<ResponseBody>> stringResponsePair) {
                    try {
                        String htmlString = stringResponsePair.second.body().string();
                        List<String> titleList = ConvertJSONStringUtil.getContent(htmlString,
                                ConvertFormatType.HTML_TAG_TITLE_FORMAT);

                        Link link = new Link(stringResponsePair.first, ((titleList != null
                                && !titleList.isEmpty()) ? titleList.get(0) : ""));

                        links.add(link);
                    } catch (IOException e) {
                        Log.e("error", e.getMessage());
                        e.printStackTrace();
                    }
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    Log.e("error", throwable.getMessage());
                    new AlertDialog.Builder(getActivity())
                            .setMessage(throwable.getMessage())
                            .setTitle(R.string.error)
                            .setPositiveButton(R.string.generic_OK, null)
                            .show();
                    container.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    gsonSetup(mentions, emoticons, new ArrayList<Link>());
                }
            }, new Action0() {
                @Override
                public void call() {
                    updateProgressBar(false);
                    gsonSetup(mentions, emoticons, links);
                }
            });
        } else {
            updateProgressBar(false);
            gsonSetup(mentions, emoticons, new ArrayList<Link>());
        }
    }

    /**
     * Method used to setup GSON object for display after conversion
     * @param mentions list of mentions from chat message
     * @param emoticons list of emotions from message
     * @param links list of urls from chat message
     */
    private void gsonSetup(List<String> mentions, List<String> emoticons, List<Link> links) {
        MessageContent messageContent = new MessageContent(mentions, emoticons, links);

        Gson gson = new Gson();
        viewModel.setDisplayJSON(gson.toJson(messageContent));
        viewModel.setDisplayMessage(viewModel.getInputMessage());

        updateUI();
    }

    private void updateProgressBar(boolean shownProgress) {
        container.setEnabled(!shownProgress);
        progressBar.setVisibility(shownProgress ? View.VISIBLE : View.GONE);
    }

}

package com.altassian.hipchatconvert.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.altassian.hipchatconvert.R;


/**
 * Activity for user input chat message
 *
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

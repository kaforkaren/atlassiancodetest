package com.altassian.hipchatconvert.Activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.altassian.hipchatconvert.R;

import butterknife.ButterKnife;

/**
 * Activity for display message / JSON string in full
 *
 */
public class DataDetailsActivity extends AppCompatActivity {

    /**
     * Intent tag for use passing data string
     */
    public final static String DATA_DISPLAY_KEY = "DATA_DISPLAY_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_details);

        ButterKnife.bind(this);

    }

    /**
     * Method to generate intent with passing data string
     * @param context Context
     * @param dataInString data string to display
     * @return intent object that use to display on next screen
     */
    public static Intent buildIntent(Context context, String dataInString) {
        Intent intent = new Intent(context, DataDetailsActivity.class);
        intent.putExtra(DATA_DISPLAY_KEY, dataInString);
        return intent;
    }
}

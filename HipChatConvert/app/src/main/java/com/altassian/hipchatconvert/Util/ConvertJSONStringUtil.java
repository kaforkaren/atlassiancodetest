package com.altassian.hipchatconvert.Util;

import android.util.Pair;

import com.altassian.hipchatconvert.Network.ClientService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Utility Class to convert chat message
 *
 */
public class ConvertJSONStringUtil {

    /**
     * Method used to convert chat message
     * @param message chat message
     * @param convertFormatType type of format that message would convert
     * @return list of matched string
     */
    public static List<String> getContent(String message, ConvertFormatType convertFormatType) {
        Set<String> resultSet = new LinkedHashSet<>();
        if (message != null && !message.isEmpty()) {
            Pattern pattern = Pattern.compile(convertFormatType.getConvertFormat().getRegExp());
            Matcher matcher = pattern.matcher(message);

            while (matcher.find()) {
                String matchedString = matcher.group();
                matchedString = matchedString.substring(convertFormatType.getConvertFormat().getStartPosition(),
                        matchedString.length() - convertFormatType.getConvertFormat().getNumberCharsToEnd());
                resultSet.add(matchedString.trim());
            }
        }

        String[] results = new String[resultSet.size()];
        results = resultSet.toArray(results);
        List<String> resultList = Arrays.asList(results);
        return resultList;
    }

    /**
     * Method used to get url title
     * @param linksInString list of url string
     * @param successAction success action block
     * @param errorAction error action block
     * @param completeAction complete action block
     */
    public static void getUrlTitle(List<String> linksInString, Action1<Pair<String, Response<ResponseBody>>> successAction,
                                   Action1<Throwable> errorAction, Action0 completeAction) {
        Observable.from(linksInString).flatMap(new Func1<String, Observable<Pair<String, Response<ResponseBody>>>>() {
            @Override
            public Observable<Pair<String, retrofit2.Response<ResponseBody>>> call(String s) {
                return ClientService.getInstance().fetchUrl(s);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successAction, errorAction, completeAction);
    }

}

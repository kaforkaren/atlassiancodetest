package com.altassian.hipchatconvert.Util;

import com.altassian.hipchatconvert.Pojo.ConvertFormat;

public enum ConvertFormatType {
    MENTION_CONVERT_FORMAT(Constants.MENTION_REG, 1, 0),
    EMOTION_CONVERT_FORMAT(Constants.EMOTICONS_REG, 1, 1),
    LINK_CONVERT_FORMAT(Constants.URL_REG, 0, 0),
    HTML_TAG_TITLE_FORMAT(Constants.HTML_TITLE_REG, 7, 8);

    ConvertFormat convertFormat;

    ConvertFormatType(String regularExp, int startPosition, int numberCharsToEnd) {
        this.convertFormat = new ConvertFormat(regularExp, startPosition, numberCharsToEnd);
    }

    public ConvertFormat getConvertFormat() {
        return convertFormat;
    }
}

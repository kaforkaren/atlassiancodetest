package com.altassian.hipchatconvert.ViewModel;

import lombok.Getter;
import lombok.Setter;

/**
 * Model class for MainActivity
 */
public class MainViewModel {
    @Getter @Setter String inputMessage;

    @Setter @Getter String displayJSON;
    @Setter @Getter String displayMessage;
}

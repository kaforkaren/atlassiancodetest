# README #

This application is simple application that takes a chat message string and returns a JSON string with rules:

@ mention
{"mentions": ["username"]}

(emoticons)
{"emoticons": ["success"]}

url
{"links": [{"url":"url", "title":"title"}]}

### Setup ###
Just simply checkout master branch and import the project in Android studio.

It requires the device in API level 16 (Jelly Bean) above.


### Who do I talk to? ###
Create by Karen Kung (karen.kkung@gmail.com)